<div id="header-container" class="wrap">
   <h2><?php _e('Settings', 'dailymotion'); ?></h2>
   <?php if(!empty($options3) || !empty($options1)) : ?>
   <p><?php  _e('Congrats, you\'re now connected!', 'dailymotion'); ?><br/>
      <?php  _e('You can start managing your videos using the Dailymotion menus to the left.', 'dailymotion'); ?>
   </p>
   <?php else : ?>
   <p><?php  _e('Insert your video into your post while you writing.', 'dailymotion'); ?><br/>
      <?php  _e('Connect Your Dailymotion account(s) below.', 'dailymotion'); ?>
   </p>
   <?php endif; ?>
   <div class="box_wrapper">
      <?php if(isset($dailymotionData) && !empty($dailymotionData) && is_array($dailymotionData)) : ?>
      <div id="dailymotion_box">
         <div class="align_center">
            <div class="header_logo first"><img src="<?php print DAILYMOTION_URL; ?>/img/dm_head.jpg" alt="" /></div>
            <div id="dailymotion_box_conected">
               <div class="right_arraow"><img src="<?php print DAILYMOTION_URL; ?>/img/right_sign.jpg" alt="" /></div>
               <div class="connected"><?php _e('Your Dailymotion account is connected', 'dailymotion'); ?></div>
            </div>
            <div id="dailymotion_box_conected" class="conected_second">
               <div class="account_picture"><img src="<?php print $dailymotionData['user_photo']; ?>" alt="" /></div>
               <div class="account_name"><?php print $dailymotionData['screenname']; ?></div>
               <div class="disconnect_wrapper">
                  <a class="disconnect_account" rel="dailymotion" href="#"><?php _e('Disconnect', 'dailymotion'); ?></a>
                  <img id="dailymotion_throbber" src="<?php print DAILYMOTION_URL; ?>/img/throbber.gif" alt="" class="displaynone" />
               </div>
               <div class="total_video">
                  <div><?php _e('Total videos'); ?></div>
                  <div class="span_count"><?php  print $dailymotionData['total_record']; ?></div>
               </div>
               <div class="total_video">
                  <div><?php _e('Last uploaded'); ?></div>
                  <div class="span_count"><?php  print $dailymotionData['last_uploaded']; ?></div>
               </div>
            </div>
         </div>
         <div id="header-container" class="wrap user_publish_setting">
            <h2 class="blankh2">&nbsp;</h2>
            <div class="wrap">
               <h2><?php _e('Publication Settings', 'dailymotion'); ?></h2>
               <?php settings_errors(); ?>
               <form id="publication_settings_form" method="post" action="options.php" onsubmit="return publication_settings_form_submit(this);">
                  <?php settings_fields('publish_id_option_group'); do_settings_sections('publish-id-setting-admin'); ?>
                  <div class="wht_pub">
                     <div class="left_pub"></div>
                     <div class="right_pub">
                        <h3><?php _e('What is Publisher?', 'dailymotion'); ?></h3>
                        <p>Dailymotion Publisher allows you to earn advertising revenue when sharing Dailymotion videos on your site.</p>
                        <p><a target="_balnk" href="http://publisher.dailymotion.com/">Join Publisher now</a> - it's free!</p>
                     </div>
                  </div>
                  <div id="publicationmessage"></div>
                  <?php submit_button('Save Publication Settings'); ?>
               </form>
            </div>
         </div>
      </div>
      <?php else: ?>
      <div id="dailymotion_box">
         <div class="align_center">
            <div class="header_logo first"><img src="<?php print DAILYMOTION_URL; ?>/img/dm_head.jpg" alt="" /></div>
            <?php if(empty($options4)) : ?>
            <a class="dm_pop_btn" id="dm_auth_popup" href="#dailymotion_form_popup"><?php _e('Connect to Dailymotion', 'dailymotion'); ?></a>
            <?php else : ?>
            <a class="dm_pop_btn" href="<?php print $dailymotionData; ?>"><?php _e('Connect to Dailymotion', 'dailymotion'); ?></a>
            <?php endif; ?>
            <a class="sub_link" id="sub_link" target="_balnk" href="#dailymotion_form_popup"><?php _e('or create a dailymotion account', 'dailymotion'); ?></a>
         </div>
      </div>
      <div style="display: none">
         <div id="dailymotion_form_popup" style="float:left;">
            <div class="connect_heading">Connect to Dailymotion.com</div>
            <div class="wrap11">
               <form id="dailymotion_outh_form" method="post" action="options.php" onsubmit="return dailymotion_settings_form_submit(this);">
                  <?php settings_fields('dailymotion_option_group'); do_settings_sections('dailymotion-outh-setting'); ?>
                  <div id="dailymotion_message" style="display:none"></div>
                  <?php submit_button('Save'); ?>
                  <p class="submit">Don't have an API Key yet? <a href="javascript:void(0);" class="show_account_desc">Create one</a></p>
               </form>
               <div class="create_account_desc">
                  <h3>How to create an API Key</h3>
                  <ol>
                     <li>Connect with your Dailymotion account on <br> <a target="_balnk" href="http://www.dailymotion.com/profile/developer">http://www.dailymotion.com/profile/developer</a></li>
                     <li>If necessary, click on the button "Create a new API Key" to create a new form</li>
                     <li>
                        Enter the following info in the form: <br>
                        <ul>
                           <li>Name of you app: "My wordpress/Drupal app"</li>
                           <li>Application website url: <a href="javascript:void(0);"><?php bloginfo('url'); ?></a></li>
                           <li>Callback url: <a href="javascript:void(0);"><?php print admin_url('admin.php').'?page=dm-admin-setting'; ?></a></li>
                        </ul>
                     </li>
                     <li>Save the form</li>
                     <li>Copy your API key and secret in the <a href="javascript:void(0);" class="hide_account_desc">Form here</a></li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <?php endif; ?>
      <?php if(isset($options1) && !empty($options1)) : ?>
      <div id="dailymotion_box" class="dmc-box">
         <div class="align_center">
            <div class="header_logo"><img src="<?php print DAILYMOTION_URL; ?>/img/dm_cloud.jpg" alt="" /></div>
            <div id="dailymotion_box_conected">
               <div class="right_arraow"><img src="<?php print DAILYMOTION_URL; ?>/img/right_sign.jpg" alt="" /></div>
               <div class="connected"><?php _e('Your Dailymotion Cloud account is connected', 'dailymotion'); ?></div>
            </div>
            <div id="dailymotion_box_conected" class="conected_second">
               <?php $data = $this->showCloudaccountData(); ?>
               <div class="account_name"><?php print $data['udata']; ?></div>
               <div class="disconnect_wrapper">
                  <a class="disconnect_account" rel="cloud" href="#"><?php _e('Disconnect', 'dailymotion'); ?></a>
                  <img id="cloud_throbber" src="<?php print DAILYMOTION_URL; ?>/img/throbber.gif" alt="" class="displaynone" />
               </div>
               <div class="total_video">
                  <div><?php _e('Total videos'); ?></div>
                  <div class="span_count"><?php  print $data['mdata']; ?></div>
               </div>
               <div class="total_video">
                  <div><?php _e('Last uploaded'); ?></div>
                  <div class="span_count"><?php  print $data['last_uploaded']; ?></div>
               </div>
            </div>
         </div>
      </div>
      <?php else: ?>
      <div id="dailymotion_box" class="cloudbox">
         <div class="align_center">
            <div class="header_logo"><img src="<?php print DAILYMOTION_URL; ?>/img/dynaamo-logo.png" alt="" /></div>
            <a id="cloud_form_link" class="cloud_pop_btn" href="#cloud_form_popup"><?php _e('Connect to your Dynaamo SmartCloud Account', 'dailymotion'); ?></a>
            <a class="sub_link" id="cloud_register_trigger" href="javascript:void(0);"><?php _e('<strong>Your videos are hosted for Free but are public.</strong>', 'dailymotion'); ?></a>
         </div>
      </div>
      <?php endif; ?>
   </div>
   <?php if(empty($options1)) : ?>
   <?php //settings_errors(); ?>
   <div style="display: none">
      <div id="cloud_form_popup" style="float:left;">
         <div class="connect_heading"><?php _e('Connect to Dailymotion Cloud', 'dailymotion'); ?></div>
         <div class="wrap11">
            <form id="cloud_settings_form" method="post" action="options.php" onsubmit="return cloud_settings_form_submit(this);">
               <?php settings_fields('dm_cloud_option_group'); do_settings_sections('my-setting-admin'); ?>
               <div id="message"></div>
               <?php submit_button('Connect my account'); ?>
            </form>
         </div>
      </div>
   </div>
   <?php endif; ?>
</div>