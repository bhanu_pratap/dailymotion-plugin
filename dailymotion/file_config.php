<?php

//All method related to dailymotion clod
require_once(DAILYMOTION_DIR . '/libraries/class.error.php');

//All method related to dailymotion clod
require_once(DAILYMOTION_DIR . '/libraries/class.cloud.php');

//All method related to dailymotion.com
require_once(DAILYMOTION_DIR . '/libraries/class.dailymotion.php');

//MAin setting form
require_once(DAILYMOTION_DIR . '/common/dailymotion_main.php');

//Dailymotion cloud file including
require_once(DAILYMOTION_DIR . '/dmc/dailymotion_cloud_gallery.php');

//Dailymotion file including
require_once(DAILYMOTION_DIR . '/dm/dailymotion_gallery.php');

//Metabox file including
require_once(DAILYMOTION_DIR . '/common/metabox.php');

//Dailymotion file including
require_once(DAILYMOTION_DIR . '/video_upload/video_upload.php');

//Dailymotion file including
//require_once(DAILYMOTION_DIR . '/dmc-userauth.php');