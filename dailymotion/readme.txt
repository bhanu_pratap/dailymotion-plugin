Description:

This plugin  shows Dailymotion, Dailymotion cloud videos while you write your post. Pick related video and insert into your post.You save time and become a member of our network. This plugin provides a user frindly interface, a click allows you to insert related videos into your post. 
The video will appear on your website normally but once someone hovers over the video social share buttons will appear. To view the video they have to share it they can like it on facebook google plus 1 or tweet it on twitter. Once they share the video they can watch the video. It have ability to search for related videos. You will save time and be more efficient.It provides a seprate menu and pages to remove any confusion and to be more user friendly. You can manage your videos on Dailymotion.com and Dailymotion Cloud through a simple interface on your website on you are insatalling this plugin.
This Plugin will help you get more social shares to your website. With this wordpress plugin you can post a video to your Dailymotion.com and Dailymotion Cloud account. 

Dailymotion - over 30 million videos at your fingertips - there's something for everyone!
1. Watch popular videos in News, Music, Cinema, TV, ...
2. Discover new amazing videos every day
3. Search among more than 30 millions videos
4. Access your own videos
5. Enjoy a stunning fullscreen experience in High Definition
6. Capture and upload your videos

Installation:

1. Unzip to wp-content/plugins on your server. 
2. Activate through the 'Plugins' menu in WordPress 
3. Configure your account settings from user-setting page.
4. Start writing a new post! Dailymotion will suggest related videos, when you edit a post! 
5. Mouse over on videos thumbnail and click on "Insert" or "Insert into post" button to insert video into post.
6. It genrates a short code for clicked video and plays video while you publish your post.
